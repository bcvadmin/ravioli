/*
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import java.io.{FileInputStream, BufferedInputStream, File}
import org.objectweb.asm.ClassReader

object TestClassReaderFactory {

  def classReaderFor(testClass: Class[_ <: Any]) = {
    val classFileName = getFileName(testClass)
    var file = new File("build/classes/test/" + classFileName)   // Gradle
    if (file.exists()) {
      file
    }
    else {
      file = new File("out/test/ravioli/" + classFileName)       // IntelliJ
      if (!file.exists())
        throw new Exception("Cannot find class file for " + testClass + "; maybe you should add your IDE's output dir to the 'classReaderFor' method...")
    }

    new ClassReader(new BufferedInputStream(new FileInputStream(file)))
  }

  def getFileName(clazz: Class[_ <: Any]) = {
    if (clazz.getDeclaringClass() == null) {
      clazz.getCanonicalName.replace('.', '/') + ".class"
    }
    else {
      clazz.getDeclaringClass().getCanonicalName.replace('.', '/') + "$" + clazz.getSimpleName() + ".class"
    }
  }
}
