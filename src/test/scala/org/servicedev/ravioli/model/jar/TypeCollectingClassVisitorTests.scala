/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import org.scalatest.{Matchers, FunSuite}
import org.scalatest.mock.MockitoSugar
import java.io.{File, FileInputStream, BufferedInputStream}
import org.objectweb.asm.ClassReader


class TypeCollectingClassVisitorTests extends FunSuite with Matchers with MockitoSugar {

  test("An empty class should only have a used type for java.lang.Object") {
    val classReader = classReaderFor(classOf[Aap])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object]))
  }

  test("A class extending a base class should have a used type for that base class") {
    val classReader = classReaderFor(classOf[Noot])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Aap]))
  }

  test("A class with a member variable should have a used type for that member") {
    val classReader = classReaderFor(classOf[Mies])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Object].getName(), classOf[Aap].getName(), classOf[Noot].getName())
  }

  test("A class with a array type member variable should have a used type for that member") {
    val classReader = classReaderFor(classOf[Wim])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should contain (classOf[Aap].getName())
  }

  test("A method parameter should lead to a used type") {
    val classReader = classReaderFor(classOf[Wim])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Object].getName(), classOf[Mies].getName(), classOf[Noot].getName())
  }

  test("A method result should lead to a used type") {
    val classReader = classReaderFor(classOf[Zus])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Noot], classOf[Aap]))
  }

  test("Local variable in method should lead to a used type") {
    val classReader = classReaderFor(classOf[Jet])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Wim], classOf[Zus]))
  }

  test("Instanceof test should lead to used type") {
    val classReader = classReaderFor(classOf[Teun])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Jet]))
  }

  test("Field access should lead to used type") {
    val classReader = classReaderFor(classOf[Vuur])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Mies], classOf[Aap]))
  }

  test("Invoke of static method should lead to used type") {
    val classReader = classReaderFor(classOf[Lam])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Gijs], classOf[Aap]))
  }

  test("Creation of multi-dimension array should lead to used type") {
    val classReader = classReaderFor(classOf[Kees])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Teun]))
  }

  test("Method throwing exception should lead to used type") {
    val classReader = classReaderFor(classOf[Wei])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Bok]))
  }

  test("Catching exception should lead to used type") {
    val classReader = classReaderFor(classOf[Does])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object], classOf[Wei], classOf[Bok]))
  }

  test("Finally block should lead to used type") {
    val classReader = classReaderFor(classOf[Hok])
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)

    typeCollector.types should equal (typesOf(classOf[Object]))
  }

  test("Class only using types in the same package, should not refer any package") {
    val typeCollector = new TypeCollectingClassVisitor()
    classReaderFor(classOf[Jet]).accept(typeCollector, 0)

    typeCollector.referredPackages should have size 0
  }

  test("Types used by inner classes should be detected") {
    val typeCollector = new TypeCollectingClassVisitor()
    val clazz: Class[_ <: Any] = classOf[Outer#Inner]
    classReaderFor(clazz).accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Mies].getName(), classOf[Aap].getName(), classOf[Noot].getName())
  }

  test("Annotations on classes, methods and fields should be detected") {
    val typeCollector = new TypeCollectingClassVisitor()
    classReaderFor(classOf[Annotated]).accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Annotation3].getName(), classOf[Annotation1].getName(), classOf[Annotation2].getName())
  }

  test("Annotation member types should be detected") {
    val typeCollector = new TypeCollectingClassVisitor()
    classReaderFor(classOf[Annotation3]).accept(typeCollector, 0)
    classReaderFor(classOf[Annotation5]).accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Bool].getName(), classOf[Annotation4].getName())
  }

  test("Annotation types themselves") {
    val typeCollector = new TypeCollectingClassVisitor()
    classReaderFor(classOf[Annotated]).accept(typeCollector, 0)

    typeCollector.types should contain allOf (classOf[Bool].getName(), classOf[Annotation4].getName())
  }

  private def classReaderFor(testClass: Class[_ <: Any]) = TestClassReaderFactory.classReaderFor(testClass)

  private def typesOf(classes: Class[_ <: Any]*): Set[String] = {
    classes.map { _.getName() }.toSet
  }
}
