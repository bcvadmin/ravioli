/*
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar;

/**
 * This file defines a bunch of simple test classes, used in TypeCollectingClassVisitorTests.
 * It is written in Java, because not all bytecode instructions can be generated from Scala source, e.g. field access
 * in Scala is not compiled to 'getfield', but to 'invokespecial'.
 */
class Aap {
}

class Noot extends Aap {
}

class Mies {
    Aap aap = new Noot();
}

class Wim {
  void method(Mies m, Noot[] noot) {}
  Aap[] aap;
}

class Zus {
  Noot method() {
      return null;
  }
  Aap[] other() {
      return null;
  }
}

class Jet {
  void method() {
    Wim w = null;
    Zus z = null;
  }
}

class Teun {
  void method(Object o) {
    boolean b = o instanceof Jet;
  }
}

class Vuur {
  void method(Mies m) {
    Object a = m.aap;   // Value must be declared as Object; otherwise, the 'Aap' type will be detected from the local variable instead of the field access.
  }
}

class Gijs {
    static Aap getAap() {
        return new Aap();
    }
}

class Lam {
    void method() {
        Object a = Gijs.getAap();
    }
}

class Kees {
    void method() {
        Object[][] a = new Teun[2][3];
    }
}

class Bok extends Exception {
}

class Wei {
    void method() throws Bok {
    }
}

class Does {
    void method() {
        try {
            new Wei().method();
        }
        catch (Bok b) {
        }
    }
}

class Hok {
    void method() {
        try {
        }
        finally {
        }
    }
}

class Outer {
    class Inner {
        Aap aap = new Noot();
        void method(Mies m) {}
    }
}

enum Bool { TRUE, FALSE };

@interface Annotation1 {}
@interface Annotation2 {}
@interface Annotation3 {
    Bool bool();
}
@interface Annotation4 {}
@interface Annotation5 {
    Annotation4[] value() default{};
}

@Annotation1
class Annotated {
    @Annotation2
    void method() {}

    @Annotation3(bool = Bool.FALSE)
    String field;

    @Annotation5({ @Annotation4 })
    String another;
}