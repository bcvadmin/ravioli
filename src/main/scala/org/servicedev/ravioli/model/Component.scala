/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model

import java.util.List
import scala.collection.immutable.Map

/**
 * The core element of the <code>ComponentModel</code>.
 * The <code>ComponentModel</code> is comprised of components and only components can have dependencies (to other components).
 * Note that a component is not aware of its incoming dependencies, but the <code>ComponentModel</code> can compute them.
 */
abstract trait Component {

  /**
   * Component identifier
   */
  def getIdentifier(): String
  
  /**
   * Component label (defaults to identifier)
   */
  def getLabel() : String = {
    getIdentifier
  }

  /**
   * Component's outgoing dependencies
   */
  def getOutgoingDependencies(): List[Dependency]
  
  /**
   * Return properties 
   */
  def getProperties() : Option[Map[String, String]] = {
    None
  }
}