/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import org.objectweb.asm._
import scala.collection.mutable
import scala.collection.immutable.HashMap

/**
 * An ASM class visitor that will extract all used types from a class.
 */
class TypeCollectingClassVisitor extends ClassVisitor(Opcodes.ASM4) {

  var className: String = null
  val packageTypeMapping = new mutable.HashMap[String, mutable.Set[String]] with mutable.MultiMap[String, String]
  var packageName: String = null

  /**
   * Returns all the types that are used by the visited class.
   */
  def types(): Set[String] = {
    // Strange: flattening an Iterable[Set] results in List. To get rid of the duplicates, convert to Set again.
    packageTypeMapping.values.flatten.toSet
  }

  /**
   * Returns all the packages that are used by the visited class.
   */
  def referredPackages(): Map[String, Set[String]] = {
    packageTypeMapping.filter {
      entry => entry._1 != packageName &&  ! entry._1.startsWith("java.")
    }.map {
      entry => (entry._1, entry._2.toSet)
    }.toMap
  }

  override def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]) {
    className = Type.getObjectType(name).getClassName
    packageName = Type.getObjectType(name).getClassName.split('.').init.mkString(".")
    val superType = Type.getObjectType(superName)
    addType(superType)
    interfaces.foreach { i => addType(Type.getObjectType(i)) }
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor = {
    val fieldType = Type.getType(desc)
    addType(fieldType)
    return new TypeCollectingFieldVisitor()
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
    val returnType = Type.getReturnType(desc)
    addType(returnType)
    Type.getArgumentTypes(desc).foreach { argType =>
      addType(argType)
    }
    if (exceptions != null)
      exceptions.foreach { exc =>
        addType(Type.getObjectType(exc));
      }
    return new TypeCollectingMethodVisitor()
  }

  override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
    addType(Type.getType(desc))
    return new TypeCollectingAnnotationVisitor()
  }

  private def addType(javaType: Type) {
    if (javaType.getSort() == Type.OBJECT) {
      val className = javaType.getClassName()
      val packageName = className.split('.').init.mkString(".")
      packageTypeMapping.addBinding(packageName, className)
    }
    else if (javaType.getSort == Type.ARRAY) {
      addType(javaType.getElementType())
    }
  }

  class TypeCollectingFieldVisitor extends FieldVisitor(Opcodes.ASM4) {
    override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
      addType(Type.getType(desc))
      return new TypeCollectingAnnotationVisitor()
    }
  }

  class TypeCollectingMethodVisitor extends MethodVisitor(Opcodes.ASM4) {

    override def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int): Unit = {
      if (name != "this") {
        val localVariableType = Type.getType(desc)
        addType(localVariableType)
      }
    }

    override def visitTypeInsn(opcode: Int, typeOperand: String): Unit = {
      val usedType = Type.getObjectType(typeOperand)
      addType(usedType)
    }

    override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String): Unit = {
      val classType = Type.getObjectType(owner)
      addType(classType)
      val fieldType = Type.getType(desc)
      addType(fieldType)
    }

    override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String): Unit = {
      val classType = Type.getObjectType(owner)
      addType(classType)
      val fieldType = Type.getReturnType(desc)
      addType(fieldType)
    }

    override def visitMultiANewArrayInsn(desc: String, dims: Int): Unit = {
      val elementType = Type.getObjectType(desc).getElementType()
      addType(elementType)
    }

    override def visitTryCatchBlock(start: Label, end: Label, handler: Label, exception: String): Unit = {
      if (exception != null) { // Will be null when processing finally block
        val exceptionType = Type.getObjectType(exception)
        addType(exceptionType)
      }
    }

    override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
      addType(Type.getType(desc))
      return new TypeCollectingAnnotationVisitor()
    }
  }

  class TypeCollectingAnnotationVisitor extends AnnotationVisitor(Opcodes.ASM4) {

    override def visitAnnotation(name: String, desc: String): AnnotationVisitor = {
      addType(Type.getType(desc))
      return null
    }

    override def visitArray(name: String): AnnotationVisitor = {
      return this
    }

    override def visitEnum(name: String, desc: String, value: String): Unit = {
      addType(Type.getType(desc))
    }
  }
}
