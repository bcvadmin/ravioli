/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import org.servicedev.ravioli.model.{Dependency, Component, ComponentModel}
import scala.collection.mutable
import org.servicedev.ravioli.model.base.{DefaultDependency, DefaultComponent}
import scala.collection.JavaConversions._

/**
 * A component model based on packages. Each component represents a (Java) package, a component depends on another
 * component if component's package imports / uses classes from the other package.
 *
 * @param analyzer    class analyzer that will provide the packages
 */
class PackageComponentModel(analyzer: ByteCodeAnalyzer) extends ComponentModel {

  val components = createModel()

  def getComponents = components

  def createModel(): List[Component] = {
    val packageComponents = new mutable.HashMap[String, JavaPackageComponent]

    analyzer.allPackages.foreach { pckg: String =>
      packageComponents.put(pckg, new JavaPackageComponent(pckg))
    }

    analyzer.allPackages.foreach { pckg: String =>
      val currentComponent = packageComponents(pckg)
      analyzer.dependentPackages(pckg).foreach { dependent: String =>
        val targetComponent = packageComponents.get(dependent)
        if (targetComponent.isDefined) {
          val dep = new JavaTypeDependency(currentComponent, targetComponent.get, analyzer.usedTypes(pckg, dependent))
          currentComponent.addOutgoingDependency(dep)
        }
        else {
          println("unsatisfied dep: " + dependent)
        }
      }
    }
    packageComponents.values.toList
  }

  /**
   * Explains a number of dependencies, for human consumption. The returned string should be HTML-formatted.
   * Note that the dependencies can have different source and/or target components, as the caller might be interested
   * in the dependencies between groups of components.
   *
   * @param dependencies  the dependencies to explain
   * @return  explanation or null if the model doesn't provide an explanation
   */
  override def explainDependencies(dependencies: Seq[Dependency]): String = {
    val collectedTypes = dependencies.map { _.asInstanceOf[JavaTypeDependency].getExplanation }.flatten
    if (collectedTypes.nonEmpty)
      "<html>" + collectedTypes.mkString("<br>") + "<html>"
    else
      "?"
  }
}
