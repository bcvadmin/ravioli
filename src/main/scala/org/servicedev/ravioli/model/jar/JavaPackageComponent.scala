/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import org.servicedev.ravioli.model.base.DefaultComponent

/**
 * A component representing a Java package.
 * @param identifier
 */
class JavaPackageComponent(identifier: String) extends DefaultComponent(identifier) {

  override def getProperties(): Option[Map[String, String]] = {
    Some(Map("id" -> identifier, "type" -> "Java package"))
  }
}
