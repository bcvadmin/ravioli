/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.jar

import scala.collection.mutable
import org.objectweb.asm.ClassReader

/**
 * Base class for bytecode or class analyzers; provides functions for setting up a package dependency mapping.
 */
class ByteCodeAnalyzer {

  // A dependent package with its used types
  type PackageDependencies = mutable.HashMap[String, mutable.Set[String]] with mutable.MultiMap[String, String]
  def emptyPackageDependency(): PackageDependencies = new mutable.HashMap[String, mutable.Set[String]] with mutable.MultiMap[String, String]

  // Mapping from package to package to used types
  val packageDependencyMapping = new mutable.HashMap[String, PackageDependencies]

  /**
   * Returns all packages this analyzer has analysed (which has no relation whatsoever with the packages they depend on!)
   */
  def allPackages(): Iterable[String] = {
    packageDependencyMapping.keys
  }

  /**
   * Returns all the packages that the given (analyzed) package depends on.
   */
  def dependentPackages(pckg: String): Set[String] = {
    (Set.newBuilder ++= packageDependencyMapping(pckg).keys).result()
  }

  /**
   * Returns all the types that are causing the dependency between the given packages (i.e. that are used by the fromPackage from the toPackage)
   */
  def usedTypes(fromPackage: String, toPackage: String): Set[String] = {
    packageDependencyMapping(fromPackage)(toPackage).toSet
  }

  def addTypeDependencies(className: String, usedPackages: Map[String, Set[String]]) {
    if (! usedPackages.isEmpty) {
      val fromPackage = packageName(className)
      usedPackages.foreach { usedPackage =>
        val existing: PackageDependencies = packageDependencyMapping.getOrElse(fromPackage, emptyPackageDependency())
        val becauseOfTypes = usedPackage._2
        becauseOfTypes.foreach { existing.addBinding(usedPackage._1, _) }
        packageDependencyMapping.put(fromPackage, existing)  // A no-op when existing was really existing, but a put when existing is actually new (cryptic huh? ;-))
      }
    }
  }

  def extractTypeDependencies(classReader: ClassReader) {
    val typeCollector = new TypeCollectingClassVisitor()
    classReader.accept(typeCollector, 0)
    addTypeDependencies(typeCollector.className, typeCollector.referredPackages())
  }

  def packageName(className: String) = className.split('.').init.mkString(".")
}
