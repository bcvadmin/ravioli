/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.bundle

import org.servicedev.ravioli.model.adapter.PresentationAdapter
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.model.Dependency
import org.servicedev.ravioli.model.Component
import scala.collection.immutable.List

/**
 * Presentation adapter for bundle component models.
 */
class BundleComponentModelPresentationAdapter extends PresentationAdapter {

  /**
   * Returns whether the presentation adapter is applicable for the given ComponentModel.
   */
  def isAdapterFor(componentModel : ComponentModel) : Boolean = {
    return componentModel.isInstanceOf[BundleComponentModel]
  }

  /**
   * Returns the presentation types that are used for components
   */
  def componentPresentationTypes: Seq[String] = List("bundle")

  /**
   * Returns the presentation types that are used for dependencies
   */
  def dependencyPresentationTypes: Seq[String] = List("package", "bundle", "fragment-host")

  /**
   * Returns the presentation type for the given Dependency.
   */
  def getPresentationType(dependency : Dependency) : Option[String] = {
    if (dependency.isInstanceOf[BundleDependency]) {
      Some("bundle")
    } else if (dependency.isInstanceOf[PackageDependency]) {
      Some("package")
    } else if (dependency.isInstanceOf[FragmentHostDependency]) {
      Some("fragment-host")
    } else {
      None
    }
  }
  
  /**
   * Returns the presentation type for the given Component.
   */
  def getPresentationType(component : Component) : Option[String] = {
    None
  }
}