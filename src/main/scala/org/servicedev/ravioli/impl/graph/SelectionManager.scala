/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import scala.collection.immutable.List
import org.servicedev.ravioli.impl.graph.model.GraphComponent

/**
 * GraphFrame selection manager responsible for holding the current selection, and propagating selection changes
 * to registered selection listeners.
 */
class SelectionManager {

  var selectionListeners: List[SelectionListener] = List[SelectionListener]()
  var selection: Selection = new Selection(List[GraphComponent]())
  var isNotifying: Boolean = false;

  /**
   * Adds a selection listener
   */
  def addSelectionListener(listener: SelectionListener) = {
    selectionListeners = selectionListeners :+ listener
  }

  /**
   * Removes a selection listener
   */
  def removeSelectionListener(listener: SelectionListener) = {
    selectionListeners = selectionListeners.filter(_ != listener)
  }

  /**
   * Updates the current selection
   */
  def updateSelection(source: SelectionListener, selectedComponents: List[GraphComponent]) = {
    // prevent recursion in notifying selection listeners
    if (!isNotifying) {
      val newSelection = new Selection(selectedComponents)
      if (!newSelection.getSelectedComponents.equals(selection.getSelectedComponents)) {
        selection = newSelection
        isNotifying = true
        for (listener <- selectionListeners if listener != source) {
          listener.onSelectionChanged(selection)
        }
        isNotifying = false
      }
    }
  }

  def getSelection(): Selection = {
    selection
  }

}

/**
 * SelectionListener interface to be implemented by componentens that want to be notified of a selection change.
 */
abstract trait SelectionListener {

  def onSelectionChanged(selection: Selection)

}

/**
 * Selection holding a list of selected GraphComponents.
 */
class Selection(selectedComponents: List[GraphComponent]) {

  def getSelectedComponents(): List[GraphComponent] = {
    selectedComponents
  }

}