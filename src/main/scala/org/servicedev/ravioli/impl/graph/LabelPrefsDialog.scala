/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import net.miginfocom.swing.MigLayout
import javax.swing.BorderFactory
import javax.swing.JButton
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JTextField
import java.awt.BorderLayout
import java.awt.Color
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.FocusEvent
import java.awt.event.FocusListener
import java.util.ArrayList
import java.util.List
import java.util.prefs.Preferences
import java.util.regex.Pattern
import java.util.regex.PatternSyntaxException

/**
 * Preferences dialog for configuring label replacements.
 */
class LabelPrefsDialog(frame: JFrame) extends JDialog(frame, true) {

  val helpText: String = "Enter a regular expression together with a replacement pattern for each label you want to replace. " +
    "For example, the pair 'org\\.apache\\.(.*)', '$1' will strip 'org.apache' from the names."

  private var regexPanel: JPanel = null
  private var moreButton: JButton = null
  private var regexs: List[JTextField] = new ArrayList[JTextField]
  private var replacements: List[JTextField] = new ArrayList[JTextField]
  private var nrOfStoredPrefs: Int = 0

  createDialog()

  def createDialog() {
    regexPanel = new JPanel(new MigLayout)
    regexPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(9, 9, 9, 9), BorderFactory.createEtchedBorder))
    regexPanel.add(new JLabel("<html>" + helpText + "</html>"), "span, wrap, wmax 90%, gapy 0 10")
    nrOfStoredPrefs = loadPrefs
    if (nrOfStoredPrefs == 0) {
      addLine
    }
    addLine
    moreButton = new JButton("More")
    moreButton.addActionListener(new ActionListener {
      def actionPerformed(actionEvent: ActionEvent) {
        regexPanel.remove(moreButton)
        addLine
        regexPanel.add(moreButton)
        LabelPrefsDialog.this.pack
      }
    })
    regexPanel.add(moreButton, "wrap")
    val bottom: JPanel = new JPanel
    val okButton: JButton = new JButton("OK")
    okButton.addActionListener(new ActionListener {
      def actionPerformed(actionEvent: ActionEvent) {
        val error: String = validateInput
        if (error != null) {
          JOptionPane.showMessageDialog(LabelPrefsDialog.this, error, "Error", JOptionPane.DEFAULT_OPTION)
        }
        else {
          savePrefs
          setVisible(false)
        }
      }
    })
    bottom.add(okButton)
    val panel: JPanel = new JPanel(new BorderLayout)
    panel.add(regexPanel, BorderLayout.CENTER)
    panel.add(bottom, BorderLayout.SOUTH)
    setContentPane(panel)
    getRootPane.setDefaultButton(okButton)
    pack
  }

  def getReplacements: List[String] = {
    val result: List[String] = new ArrayList[String]
    for (i <- 0 until regexs.size) {
      val regex: String = regexs.get(i).getText.trim
      val replacement: String = replacements.get(i).getText.trim
      if (! (regex == "") && !(replacement == "")) {
        result.add(regex)
        result.add(replacement)
      }
    }
    return result
  }

  private def addLine {
    regexPanel.add(new JLabel("Match regex:"))
    val regexField: JTextField = new JTextField(20)
    regexField.addFocusListener(new FocusListener {
      def focusGained(focusEvent: FocusEvent) {
        focusEvent.getComponent.setForeground(Color.BLACK)
      }

      def focusLost(focusEvent: FocusEvent) {
        val field: JTextField = focusEvent.getComponent.asInstanceOf[JTextField]
        if (!validRegex(field.getText)) {
          field.setForeground(Color.RED)
        }
      }
    })
    regexs.add(regexField)
    regexPanel.add(regexField)
    regexPanel.add(new JLabel("Replacement pattern:"))
    val replaceField: JTextField = new JTextField(10)
    replacements.add(replaceField)
    regexPanel.add(replaceField, "wrap")
  }

  private def validateInput: String = {
    for (i <- 0 until regexs.size) {
      if ((regexs.get(i).getText.trim == "") && !(replacements.get(i).getText.trim == "") ||
        !(regexs.get(i).getText.trim == "") && (replacements.get(i).getText.trim == ""))
        return "Each regex must have a matching replacement and vice versa."
    }
    import scala.collection.JavaConversions._
    for (regex <- regexs) {
      if (!validRegex(regex.getText)) {
        return "Invalid regular expression."
      }
    }
    return null
  }

  private def validRegex(regex: String): Boolean = {
    try {
      Pattern.compile(regex)
      return true
    }
    catch {
      case invalid: PatternSyntaxException => {
        return false
      }
    }
  }

  private def savePrefs {
    val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])
    val replacements: List[String] = getReplacements
    val count = replacements.size / 2
    for (i <- 0 until count) {
      prefs.put("labelConverterRegex" + i, replacements.get(2 * i))
      prefs.put("labelConverterReplacement" + i, replacements.get(2 * i + 1))
    }

    for (i <- count until nrOfStoredPrefs) {
      prefs.remove("labelConverterRegex" + i)
      prefs.remove("labelConverterReplacement" + i)
    }
  }

  private def loadPrefs: Int = {
    val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])

    var i = 0
    var regex: String = null
    var replacement: String = null
    do {
      regex = prefs.get("labelConverterRegex" + i, null)
      replacement = prefs.get("labelConverterReplacement" + i, null)
      if (regex != null && replacement != null) {
        addLine
        regexs.get(i).setText(regex)
        replacements.get(i).setText(replacement)
      }
      i += 1
    }
    while (regex != null && replacement != null && i < 100)

    return i
  }

}

object LabelPrefsDialog {

  def getLabelReplacementPrefs: List[String] = {
    val result: List[String] = new ArrayList[String]
    val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])

    var i = 0
    var regex: String = null
    var replacement: String = null
    do {
      regex = prefs.get("labelConverterRegex" + i, null)
      replacement = prefs.get("labelConverterReplacement" + i, null)
      if (regex != null && replacement != null) {
        result.add(regex)
        result.add(replacement)
      }
      i += 1
    }
    while (regex != null && replacement != null && i < 100)

    return result
  }

  def main(args: Array[String]) {
    val dlg: JDialog = new LabelPrefsDialog(null)
    dlg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    dlg.setVisible(true)
  }

}
