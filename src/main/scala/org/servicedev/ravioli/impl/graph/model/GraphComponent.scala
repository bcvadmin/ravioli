/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph.model

import java.util.List
import scala.Option
import scala.xml.Elem

/**
 * A component in the <code>GraphModel</code>.
 */
abstract trait GraphComponent {

  def createCopy(): GraphComponent

  def isVisible(): Boolean

  def getLabel(): String

  def getIdentifier(): String

  def getOutgoingDependencies(): List[GraphDependency]

  def getIncomingDependencies(): List[GraphDependency]

  /**
   * Adds an incoming dependency, if it's not already there.
   * @param dependency
   * @return  the dependency that is added, or if it is not added because it matched an existing, that existing (that might be modified).
   */
  def addIncomingDependency(dependency: GraphDependency): GraphDependency

  /**
   * Adds an outgoing dependency, if it's not already there.
   * @param dependency
   * @return  the dependency that is added, or if it is not added because it matched an existing, that existing (that might be modified).
   */
  def addOutgoingDependency(dependency: GraphDependency): GraphDependency

  def removeIncomingDependency(dependency: GraphDependency)

  def removeOutgoingDependency(dependency: GraphDependency)

  def setVisible(visible: Boolean)

  def getXPosition(): Double

  def getYPosition(): Double

  def getWidth(): Double

  def getHeight(): Double

  def setBounds(x: Double, y: Double, width: Double, height: Double)

  /**
   * Returns the presentation type for this graph component. TODO: explain
   * @return
   */
  def getPresentationType(): Option[String]

  def toXml(): Elem
  
  def getProperties() : Option[scala.collection.immutable.Map[String, Object]]
}