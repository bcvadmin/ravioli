/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import scala.collection.mutable.ListBuffer
import javax.swing.JTable
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import javax.swing.JViewport

/**
 * Controller responsible for handling selection changed events from the selection table
 * and for handing onSelectionChanged events from the SelectionManager.
 */
class SelectionTableSelectionController(selectionTable: JTable, selectionManager: SelectionManager) extends SelectionListener {

  /**
   * Handle a table selection change, i.e. someone selected one or multiple items in the selection table.
   */
  def handleTableSelectionChanged() = {
    val tableModel = selectionTable.getModel().asInstanceOf[SelectionTableModel]
    val selectedRows = selectionTable.getSelectedRows()
    val selectedComponents = ListBuffer[GraphComponent]()
    for (rowIndex <- selectedRows) {
      val modelRowIndex = selectionTable.convertRowIndexToModel(rowIndex)
      val graphComponent = tableModel.getGraphComponent(modelRowIndex)
      selectedComponents += graphComponent
    }
    selectionManager.updateSelection(this, selectedComponents.toList)
  }

  override def onSelectionChanged(selection: Selection) = {
    val selectionModel = selectionTable.getSelectionModel()
    selectionModel.clearSelection()
    val tableModel = selectionTable.getModel().asInstanceOf[SelectionTableModel]
    for (component <- selection.getSelectedComponents) {
      val modelRowIndex = tableModel.getRow(component)
      // convert to actual row in view, can be different due to sorting
      try {
    	val viewIndex = selectionTable.convertRowIndexToView(modelRowIndex)
        selectionModel.addSelectionInterval(viewIndex, viewIndex)
      } catch {
        case e: IndexOutOfBoundsException => {
          // ignore: table model is out of sync TODO: Find a better way to handle this
        }
      }
    }

    // scroll to first item in selection
    val viewport = selectionTable.getParent().asInstanceOf[JViewport];
    if (selectionTable.getSelectedRow() > -1) {
      val rect = selectionTable.getCellRect(selectionTable.getSelectedRow(), -1, true);
      val insets = selectionTable.getInsets();
      rect.x = insets.left;
      rect.width = selectionTable.getWidth() - insets.left - insets.right;
      selectionTable.scrollRectToVisible(rect);
    }
  }
}