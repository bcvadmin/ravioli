/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import javax.swing._
import java.awt.event._
import javax.imageio.ImageIO
import org.jgraph.JGraph
import scala.collection.JavaConversions._
import java.awt._
import org.osgi.framework.BundleContext
import scala.util.Try
import org.servicedev.ravioli.Constants
import org.servicedev.ravioli.model.NullModel
import org.servicedev.ravioli.model.base.DefaultComponentModel
import scala.collection.mutable.ListBuffer
import org.servicedev.ravioli.Ravioli
import org.jgraph.event.GraphSelectionListener
import org.jgraph.event.GraphSelectionEvent
import javax.swing.event.{ListSelectionListener, ListSelectionEvent}
import org.jgraph.graph.DefaultEdge
import org.servicedev.ravioli.impl.graph.model.GraphDependency
import org.servicedev.ravioli.model.ComponentModel
import org.servicedev.ravioli.impl.graph.model.GraphModel
import scala.util.control.Breaks._
import java.util.concurrent.ExecutionException
import java.util.zip.ZipException
import org.servicedev.ravioli.model.bundle.DirectoryScanningManifestComponentModel
import java.io.File
import javax.swing.filechooser.FileNameExtensionFilter
import java.io.IOException
import org.servicedev.ravioli.model.bundle.InContainerBundleScanner
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import com.jgraph.layout.JGraphLayout
import com.jgraph.layout.JGraphFacade
import com.jgraph.layout.organic.JGraphFastOrganicLayout
import com.jgraph.layout.graph.JGraphSimpleLayout
import com.jgraph.layout.simple.SimpleGridLayout
import java.util.ArrayList
import java.util.Collections
import org.servicedev.ravioli.impl.graph.model.CompositeGraphComponent
import org.servicedev.ravioli.model.jar.PackageComponentModel
import org.servicedev.ravioli.model.jar.JarFileScanner
import org.servicedev.ravioli.model.jar.ClassFileScanner
import org.slf4j.LoggerFactory
import org.servicedev.ravioli.model.dm.DependencyManagerComponentModel
import org.servicedev.ravioli.impl.graph.model.SimpleGraphComponent
import org.servicedev.ravioli.model.adapter.PresentationAdapterFactory
import net.miginfocom.swing.MigLayout
import scala.collection.mutable
import scala.Some
import java.util.prefs.Preferences
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.table.TableRowSorter
import java.util.regex.PatternSyntaxException
import javax.swing.border.LineBorder
import java.awt.datatransfer.StringSelection

class GraphFrame extends JFrame {

  val logger = LoggerFactory.getLogger(this.getClass())

  val FRAME_WIDTH = 1024
  val FRAME_HEIGHT = 768
  val buttonSize = new Dimension(40, 28)
  var dividerSize : Int = 0
  var model: GraphModel = new GraphModel(new NullModel(), null)
  var standalone = true
  var jGraphModelAdapter: JGraphModelAdapter = null
  var jGraph: JGraph = null
  val morpher = new JGraphLayoutMorphingManager()
  var splitPane: JSplitPane = null
  var selectionTable: JTable = null
  var selectionTableModel: SelectionTableModel = null
  var graphScrollPane: JScrollPane = null
  var sideBarVisible = true
  var bundleContext: BundleContext = null
  var labelConverter = new RegexBasedLabelConverter()
  val propertiesTableModel = new PropertiesTableModel()
  val debug = Try(System.getProperty(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY).toBoolean).getOrElse(false)
  val selectionManager = new SelectionManager()
  var selectionTableSelectionController : SelectionTableSelectionController = null
  var graphSelectionController : GraphSelectionController = null
  val propertiesTableSelectionListener = new PropertiesTableSelectionListener()
  val menuItemEnableFunctions = mutable.HashMap[JMenuItem, () => Boolean]()
  var visibleLegendWindow: JDialog = null

  {
    labelConverter.setRegexReplacements(LabelPrefsDialog.getLabelReplacementPrefs);
    initializeFrame(() => {})
  }

  def this(bundleContext : BundleContext) = {
    this()
    this.standalone = false
    this.bundleContext = bundleContext
    this.model = new GraphModel(new DefaultComponentModel(), labelConverter)
    initializeFrame(() => loadInContainerBundleDependencies())
    // uncomment next line if you want to have the DM view as default (pretty handy when you're working on it ;-))
    // initializeFrame(() => loadDependencyManagerDependencies())
  }

  def initializeFrame(postInitializeAction: () => Unit) = {
    createUI()
    postInitializeAction()
  }

  private def createUI() = {
    setSize(FRAME_WIDTH, FRAME_HEIGHT)
    setJMenuBar(createMenuBar)
    add(createToolBar, BorderLayout.NORTH)
    setTitle(Ravioli.getExtendedName)
    setLocationRelativeTo(null)
    var closeOperation = WindowConstants.DISPOSE_ON_CLOSE
    if (standalone) closeOperation = JFrame.EXIT_ON_CLOSE
    setDefaultCloseOperation(closeOperation)
    splitPane = new JSplitPane()
    getContentPane().add(splitPane)
    val graphPanel = createGraphPanel()
    val selectionPanel = createSelectionTablePanel()
    val propertiesPanel = createPropertiesTablePanel()
    splitPane.setLeftComponent(graphPanel)
    val tableSplitPane = new JSplitPane()
    val selectAndFilterPanel = new JPanel()
    val selectAndFilterPanelLayout = new MigLayout("insets 0, wrap 1")
    selectAndFilterPanel.setLayout(selectAndFilterPanelLayout)
    selectAndFilterPanel.add(createComponentFilterPanel())
    selectAndFilterPanel.add(selectionPanel, "grow,push")
    tableSplitPane.setTopComponent(selectAndFilterPanel)
    tableSplitPane.setBottomComponent(propertiesPanel)
    tableSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT)
    splitPane.setRightComponent(tableSplitPane)
    splitPane.addComponentListener(new ComponentAdapter() {
      override def componentResized(componentEvent : ComponentEvent) = {
    	  if (sideBarVisible) {
    		  splitPane.setDividerLocation(computeDividerLocation())
    	  }
      }
    })
    graphSelectionController = new GraphSelectionController(jGraph, selectionManager)
    jGraph.addGraphSelectionListener(new GraphSelectionListener() {
      def valueChanged(e : GraphSelectionEvent) = {
        graphSelectionController.handleGraphSelectionChanged()
      }
    })
    selectionTableSelectionController = new SelectionTableSelectionController(selectionTable, selectionManager)
    selectionTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      def valueChanged(e : ListSelectionEvent) = {
        if (!e.getValueIsAdjusting()) {
        	selectionTableSelectionController.handleTableSelectionChanged()
        }
      }
    })
    selectionManager.addSelectionListener(graphSelectionController)
    selectionManager.addSelectionListener(selectionTableSelectionController)
    selectionManager.addSelectionListener(propertiesTableSelectionListener)
    splitPane.setDividerLocation(computeDividerLocation())
    tableSplitPane.setDividerLocation(500)
  }
  
  private def createComponentFilterPanel() : JPanel = {
    val filterPanel = new JPanel()
    val filterPanelLayout = new MigLayout("insets 1")
    filterPanel.setLayout(filterPanelLayout)
    val filterLabel : JLabel = new JLabel("Name filter:")
    filterLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 11))
    filterPanel.add(filterLabel)
    val filterText = new JTextField(20)
    val defaultBorder = filterText.getBorder()
    // update the filter on every key release
    filterText.addKeyListener(new KeyListener() {
      def keyReleased(e : KeyEvent) = {
        try {
        val rowFilter = RowFilter.regexFilter(filterText.getText(), 1).asInstanceOf[RowFilter[SelectionTableModel, Integer]]
        val rowSorter = new TableRowSorter[SelectionTableModel](selectionTableModel)
        selectionTable.setRowSorter(rowSorter)
        rowSorter.setRowFilter(rowFilter)
        filterText.setBorder(defaultBorder)
        filterText.setToolTipText(null)
        } catch {
          // provide visual feedback when the regular expression is invalid 
          case ex : PatternSyntaxException => { filterText.setBorder(new LineBorder(Color.red)); filterText.setToolTipText("Invalid regular expression: " + ex.getMessage) }
        }
      }
      def keyPressed(e : KeyEvent) = {}
      def keyTyped(e : KeyEvent) = {}
    })
    filterPanel.add(filterText)
    filterPanel.validate()
    filterPanel
  }

  private def createGraphPanel() : JScrollPane = {
    jGraphModelAdapter = new JGraphModelAdapter(this, model)
    jGraph = new JGraph(jGraphModelAdapter) {
      override def getToolTipText(e : MouseEvent) : String = {
        val cell = jGraph.getFirstCellForLocation(e.getX(), e.getY())
        if (cell.isInstanceOf[DefaultEdge]) {
          val userObject = cell.asInstanceOf[DefaultEdge].getUserObject()
          userObject.asInstanceOf[GraphDependency].getExplanation()
        }
        else {
          null
        }
      }
    }
    ToolTipManager.sharedInstance().registerComponent(jGraph)

    graphScrollPane = new JScrollPane(jGraph)
    graphScrollPane
  }

  private def createSelectionTablePanel(): JScrollPane = {
    selectionTableModel = new SelectionTableModel(model)
    selectionTable = new JTable(selectionTableModel)
    selectionTable.setAutoCreateRowSorter(true)
    selectionTable.setGridColor(Color.BLACK);
    updateSelectionTableColumnModel()
    val scrollPane = new JScrollPane(selectionTable)
    selectionTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS)
    scrollPane.setPreferredSize(new Dimension(227, 600))
    scrollPane
  }
  
  private def updateSelectionTableColumnModel() = {
    selectionTableModel.fireTableStructureChanged()
    val columnModel = selectionTable.getColumnModel()
    columnModel.getColumn(0).setPreferredWidth(20)
    columnModel.getColumn(0).setMaxWidth(30)
    columnModel.getColumn(1).setPreferredWidth(120)
    columnModel.getColumn(1).setCellRenderer(new TooltipCellRenderer())
    columnModel.getColumn(2).setPreferredWidth(40)
    columnModel.getColumn(2).setMaxWidth(50)
    columnModel.getColumn(3).setPreferredWidth(30)
    columnModel.getColumn(3).setMaxWidth(50)  
    // give other columns a preferred width
    val columnCount = columnModel.getColumnCount()
    if (columnCount > 4) {
	    for (index <- 4 to (columnCount - 1)) {
	      columnModel.getColumn(index).setPreferredWidth(50)
	    }
    }
  }
  
  private def createPropertiesTablePanel() : JScrollPane = {
    val propertiesTable = new JTable(propertiesTableModel)
    propertiesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
    propertiesTable.addMouseListener(new MouseAdapter() {
      override def mousePressed(event: MouseEvent): Unit = {
        if (event.isPopupTrigger())
          handlePropTablePopupTrigger(propertiesTable, event)
      }})
    val scrollPane = new JScrollPane(propertiesTable)
    scrollPane.setPreferredSize(new Dimension(227, 150))
    val columnModel = propertiesTable.getColumnModel()
    columnModel.getColumn(0).setPreferredWidth(80);
    columnModel.getColumn(1).setPreferredWidth(147);
    propertiesTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS)
    // set cell renderer for tooltip on value
    columnModel.getColumn(1).setCellRenderer(new TooltipCellRenderer())
    scrollPane
  }

  private def createMenuBar() : JMenuBar = {
    val menuBar = new JMenuBar()
    val fileMenu = new JMenu("File")
    fileMenu.add(createJMenuItem("(Re)Scan OSGi Framework", () => { bundleContext != null }, () => { loadInContainerBundleDependencies() }))
    fileMenu.add(createJMenuItem("(Re)Scan DM Components", () => { bundleContext != null }, () => { loadDependencyManagerDependencies() }))
    fileMenu.add(createJMenuItem("Scan bundle manifests...", () => { true }, () => { doScanBundleManifests() }))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Analyse jar file...", null, () => { val jar = selectJarFile(); if (jar.isDefined) loadJarFilePackageDependencies(jar.get) } ))
    fileMenu.add(createJMenuItem("Analyse class files...", null, () => { val dir = selectDirectory(); if (dir.isDefined) loadClassFilePackageDependencies(dir.get) } ))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Save view state...", () => { true }, () => { saveViewState() }))
    fileMenu.add(createJMenuItem("Load view state...", () => { true }, () => { loadViewState() }))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Export to PNG...", () => { true }, () => { exportToPNG() }))
    fileMenu.addSeparator()
    fileMenu.add(createJMenuItem("Exit", () => { true }, () => { exit() }))
    menuBar.add(fileMenu)

    val editMenu = new JMenu("Edit")
    editMenu.add(createJMenuItem("Select all", null, () => { selectAll() } ))
    editMenu.add(createJMenuItem("Unselect all", null, () => { selectNone() } ))
    editMenu.add(createJMenuItem("Show all", null, () => { selectAll(); makeSelectionVisible() }))
    editMenu.add(createJMenuItem("Hide all", null, () => { selectAll(); makeSelectionInvisible() }))
    menuBar.add(editMenu)

    val viewMenu = new JMenu("View")
    viewMenu.add(createJCheckBoxMenuItem("Toggle sidebar", () => { sideBarVisible }, () => { toggleSideBar() }))
    if (debug) {
    	viewMenu.add(createJCheckBoxMenuItem("Refresh", () => { true }, () => { refreshModels() }))
    }
    menuBar.add(viewMenu)

    val preferencesMenu = new JMenu("Preferences")
    preferencesMenu.add(createJMenuItem("Labels...", () => { true }, () => { showPreferencesDialog() }))
    menuBar.add(preferencesMenu)

    val helpMenu = new JMenu("Help")
    helpMenu.add(createJMenuItem("Legend", () => { PresentationAdapterFactory.getPresentationAdapter(model.getComponentModel()).isDefined }, () => { showLegend() }))
    helpMenu.add(createJMenuItem("About " + Ravioli.getName() + "...", () => { true }, () => { showAbout() }))
    menuBar.add(helpMenu)
    menuBar
  }

  private def createToolBar() : JToolBar = {
    val toolBar = new JToolBar()
    toolBar.add(createJButton("Exit", "/icons/system-log-out-6.png", { () => exit() }))
    toolBar.add(createJButton("Force directed layout", "/icons/office-chart-polar.png", { () => applyForceDirectedLayout() }))
    toolBar.add(createJButton("Circle layout", "/icons/office-chart-ring.png", { () => applyCircleLayout() }))
    toolBar.add(createJButton("Grid layout", "/icons/distribute-horizontal-margin.png", { () => applyGridLayout() }))
    toolBar.add(createJButton("Default zoom", "/icons/zoom-original-2.png", { () => jGraph.setScale(1.0) }))
    toolBar.add(createJButton("Zoom in", "/icons/zoom-in-3.png", { () => jGraph.setScale(jGraph.getScale() * 2) }))
    toolBar.add(createJButton("Zoom out", "/icons/zoom-out-3.png", { () => jGraph.setScale(jGraph.getScale() / 2) }))
    toolBar.add(createJButton("Group selection", "/icons/format-join-node.png", { () => groupCurrentSelection() }))
    toolBar.add(createJButton("Unroup selection", "/icons/format-break-node.png", { () => unGroupCurrentSelection() }))
    toolBar.add(createJButton("Make selection visible", "/icons/format-add-node.png", { () => makeSelectionVisible() }))
    toolBar.add(createJButton("Make selection invisible", "/icons/format-remove-node.png", { () => makeSelectionInvisible() }))
    toolBar.add(createJButton("Expand outgoing dependencies", "/icons/go-next-9.png", { () => expandOutgoingDependenciesForSelection() }))
    toolBar.add(createJButton("Expand incoming dependencies", "/icons/go-previous-9.png", { () => expandIncomingDependenciesForSelection() }))
    if (debug) {
      toolBar.add(createJButton("Debug", null, { () => debugSelection() }))
    }
    toolBar
  }

  private def handlePropTablePopupTrigger(propertiesTable: JTable, event: MouseEvent) {
    val copyToClipBoard = (content:String) => {
      val selection = new StringSelection(content);
      val clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(selection, selection);
    }

    val popup = new JPopupMenu()
    popup.add(createJMenuItem("Copy", null, () => {
      val row = propertiesTable.rowAtPoint(event.getPoint())
      copyToClipBoard(propertiesTable.getValueAt(row, 0).toString + ": " + propertiesTable.getValueAt(row, 1).toString)
    }))
    popup.add(createJMenuItem("Copy all", null, () => {
      val copyContent = (0 until propertiesTable.getRowCount()).map {
        rowNr => propertiesTable.getValueAt(rowNr, 0).toString() + ": " + propertiesTable.getValueAt(rowNr, 1).toString()
      }.mkString("\n")
      copyToClipBoard(copyContent)
    }))
    popup.show(event.getComponent(), event.getX(), event.getY());
  }

  private def doScanBundleManifests() = {
    val manifestDir = selectDirectory()
    if (manifestDir.isDefined) {
      loadManifestBundleDependencies(manifestDir.get)
    }
  }

  private def selectDirectory() : Option[File] = {
    val fileSelector = new JFileChooser()
    fileSelector.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
    if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
        Some(fileSelector.getSelectedFile())
    } else {
    	None
    }
  }

  private def selectJarFile(): Option[File] = {
    val fileSelector = new JFileChooser()
    fileSelector.setFileFilter(new FileNameExtensionFilter("jar file", "jar"))
    if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
      Some(fileSelector.getSelectedFile())
    } else {
      None
    }
  }

  def loadManifestBundleDependencies(manifestDir : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new DirectoryScanningManifestComponentModel(manifestDir, this)
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  def loadModel(componentModel : ComponentModel) = {
    new LoadTask() {
      override def getComponentModel() = {
        componentModel
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  def loadJarFilePackageDependencies(jarFile : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new PackageComponentModel(new JarFileScanner(jarFile, this))
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  def loadClassFilePackageDependencies(classesDir : File) = {
    new LoadTask() {
      override def getComponentModel() = {
        new PackageComponentModel(new ClassFileScanner(classesDir, this))
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }

  private def saveViewState() = {
    val fileSelector = new JFileChooser();
    if (JFileChooser.APPROVE_OPTION == fileSelector.showSaveDialog(this)) {
      model.save(fileSelector.getSelectedFile())
    }
  }

  private def loadViewState() = {
    val fileSelector = new JFileChooser()
    if (JFileChooser.APPROVE_OPTION == fileSelector.showOpenDialog(this)) {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR))
      val start = System.currentTimeMillis()
      model.load(fileSelector.getSelectedFile())
      selectionTableModel.reset
      setCursor(Cursor.getDefaultCursor())
      if (debug) {
        logger.debug("Loading '" + fileSelector.getSelectedFile() + "' took " + (System.currentTimeMillis() - start) + " ms.")
      }
    }
  }

  private def exportToPNG() = {
    val fileSelector = new JFileChooser()
    val filter = new FileNameExtensionFilter("PNG format", "png")
    fileSelector.setFileFilter(filter)
    if (JFileChooser.APPROVE_OPTION == fileSelector.showSaveDialog(this)) {
      val imageFile = fileSelector.getSelectedFile()
      try {
        ImageIO.write(jGraph.getImage(null, 10), "png", imageFile)
      } catch {
        case ex: IOException => JOptionPane.showMessageDialog(GraphFrame.this, "Saving image file failed.", "Error", JOptionPane.ERROR_MESSAGE)
        case ex: OutOfMemoryError => JOptionPane.showMessageDialog(GraphFrame.this, "Creating image failed due to lack of memory; please restart your JVM with larger heap size.", "Error", JOptionPane.ERROR_MESSAGE)
      }
    }
  }

  private def exit() = {
    if (standalone) {
      System.exit(0)
    } else {
      GraphFrame.this.setVisible(false)
      GraphFrame.this.dispose()
    }
  }

  private def toggleSideBar() = {
    if (sideBarVisible) {
      dividerSize = splitPane.getDividerSize()
      splitPane.setDividerSize(0)
      splitPane.setDividerLocation(1.0)
      splitPane.getRightComponent().setVisible(false)
    }
    else {
      splitPane.setDividerSize(dividerSize)
      splitPane.setDividerLocation(computeDividerLocation())
      splitPane.getRightComponent().setVisible(true)
    }
    sideBarVisible = !sideBarVisible
  }

  private def refreshModels() = {
    jGraphModelAdapter.refresh
    selectionTableModel.refresh
  }

  private def showAbout() = {
    val programName = Ravioli.getName()

    val fontFamily = GraphFrame.this.getFont().getFamily()
    val aboutText = "<html><body style=\"font-family: " + fontFamily + "\"><h3>" + programName + " " + Ravioli.getVersion() + "</h3>" +
            "<p>" + programName + " is open source and licensed under LGPL.<br>" +
            "Included libraries:<br>" +
            Ravioli.getLibs().mkString("<br>") +
            "</p>" +
            "<p style=\"padding-top:10px;\">For more info see " + Ravioli.getInfoUrl() + "<p>" +
            "</html>"
    val textPane = new JTextPane()
    val backgroundColor = UIManager.getColor("OptionPane.background")
    textPane.setBackground(backgroundColor)
    textPane.setEditable(false)
    textPane.setContentType("text/html")
    textPane.setText(aboutText)

    JOptionPane.showMessageDialog(this, textPane, "About " + programName, JOptionPane.INFORMATION_MESSAGE)
  }

  private def showLegend() {
    assert(PresentationAdapterFactory.getPresentationAdapter(model.getComponentModel()).isDefined)   // Responsibility of caller
    val presentationAdapter = PresentationAdapterFactory.getPresentationAdapter(model.getComponentModel()).get

    val legendPanel = new JPanel()
    legendPanel.setLayout(new MigLayout("ins 7, wrap 2, gapx 19"))

    legendPanel.add(new JLabel("components:"), "span 2, wrap")
    presentationAdapter.componentPresentationTypes.foreach { typ =>
      legendPanel.add(new ColoredRect(jGraphModelAdapter.getNodeBackgroundColorForPresentationType(Some(typ))))
      legendPanel.add(new JLabel(typ))
    }
    legendPanel.add(new JLabel("dependencies:"), "span 2, wrap")
    presentationAdapter.dependencyPresentationTypes.foreach { typ =>
      legendPanel.add(new ColoredRect(jGraphModelAdapter.getEdgeColorForPresentationType(Some(typ))))
      legendPanel.add(new JLabel(typ))
    }

    val legendDlg = new JDialog(this, "legend", false)
    val frameLocation = getLocation()
    val frameSize = getSize()
    legendDlg.setLocation(frameLocation.x + (0.8 * frameSize.width).toInt, frameLocation.y - 10)
    legendDlg.add(legendPanel)
    legendDlg.pack()
    legendDlg.setVisible(true)
    legendDlg.addWindowListener(new WindowAdapter(){
      override def windowClosing(e: WindowEvent): Unit = {
        visibleLegendWindow.dispose()
        visibleLegendWindow = null
      }
    })
    visibleLegendWindow = legendDlg
  }

  class ColoredRect(color: Color) extends JComponent {
    override def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)
      val rectHeight = math.min(getSize().height - 2, 15)
      val baseY = getSize().height / 2 - (rectHeight / 2)
      g.setColor(color)
      g.fillRect(0, baseY, 30, rectHeight)
      g.setColor(Color.black)
      g.drawRect(0, baseY, 30, rectHeight)
    }
    override def getPreferredSize: Dimension = new Dimension(31, 15)
    override def getMinimumSize: Dimension = new Dimension(31, 10)
  }

  private def showPreferencesDialog() = {
    val dlg = new LabelPrefsDialog(this)
    dlg.setLocationRelativeTo(this)
    dlg.setVisible(true)
    labelConverter.setRegexReplacements(dlg.getReplacements)
    jGraphModelAdapter.refreshNodes
  }

  private def loadInContainerBundleDependencies() = {
    new LoadTask() {
      override def getComponentModel() = {
        new InContainerBundleScanner(bundleContext, this)
      }
      override def isComponentsDefaultVisible() = {
        true
      }
    }.execute()
  }
  
  private def loadDependencyManagerDependencies() = {
    new LoadTask() {
      override def getComponentModel() = {
        new DependencyManagerComponentModel(bundleContext)
      }
      override def isComponentsDefaultVisible() = {
        false
      }
      override def done() {
        super.done()

        // With the DependencyManagerComponentModel, components are invisible by default. This can be very confusing, so explain to the user (with "don't show me again" option)
        val prefs: Preferences = Preferences.userNodeForPackage(classOf[GraphFrame])
        if (! prefs.getBoolean("hideEmptyViewForDMComponentModel", false)) {
          // Make sure the sidebar is visible, otherwise, it's still confusing
          if (!sideBarVisible)
            toggleSideBar()
          // Show the message
          val explanationMsg = "<html><p style='width: 200px;'>For the DependencyManager component model, the graph view is initially empty. To add components to the graph, just check them in the table at the right or select a few and hit the \"Make selection visible\" button.</p></html>"
          val checkBox = new JCheckBox("never show me this again")
          JOptionPane.showMessageDialog(GraphFrame.this, Array(explanationMsg, checkBox), "Info", JOptionPane.INFORMATION_MESSAGE)
          if (checkBox.isSelected) {
            prefs.putBoolean("hideEmptyViewForDMComponentModel", true)
          }
        }
      }
    }.execute()
  }

  private def debugSelection() = {
    for (component <- selectionManager.getSelection.getSelectedComponents) {
      logger.debug("Component " + component + ":")
      logger.debug("Out:")
	  for (dep <- component.getOutgoingDependencies()) {
	    logger.debug("- " + dep.getTargetComponent() + " (" + dep.getExplanation() + ")")
	  }
	  logger.debug("In:")
	  for (dep <- component.getIncomingDependencies()) {
	    logger.debug("- " + dep.getSourceComponent() + " (" + dep.getExplanation() + ")")
	  }
    }
  }

  private def applyForceDirectedLayout() = {
    val layout = new JGraphFastOrganicLayout()
    layout.setForceConstant(200)
    layout.setInitialTemp(25)
    applyAutoLayout(layout)
  }

  private def applyCircleLayout() = {
    val layout = new JGraphSimpleLayout(JGraphSimpleLayout.TYPE_CIRCLE, 1024, 768)
    applyAutoLayout(layout)
  }

  private def applyGridLayout() = {
    val layout = new SimpleGridLayout()
    applyAutoLayout(layout)
  }

  private def applyAutoLayout(layout: JGraphLayout) = {
    val graphFacade = new JGraphFacade(jGraph)
    layout.run(graphFacade)
    val nestedMap = graphFacade.createNestedMap(true, true)
    morpher.morph(jGraph, nestedMap)
  }

  private def groupCurrentSelection() = {
    val messageType = JOptionPane.INFORMATION_MESSAGE
    val compositeIdentifier = JOptionPane.showInputDialog(GraphFrame.this,
       "Composite identifier?",
       "Create composite component", messageType)
    if (compositeIdentifier != null && !compositeIdentifier.trim().equals("")) {  // Value is null when dialog is cancelled
        if (!model.containsIdentifier(compositeIdentifier)) {
    	  doGroupCurrentSelection(compositeIdentifier)
        } else {
          JOptionPane.showConfirmDialog(GraphFrame.this, "Identifier '" + compositeIdentifier + "' is already used in this model.", "Error", JOptionPane.DEFAULT_OPTION)
        }
    }
    else if (compositeIdentifier != null) {
      JOptionPane.showConfirmDialog(GraphFrame.this, "Group identifier can't be empty.", "Error", JOptionPane.DEFAULT_OPTION)
    }
  }

  private def doGroupCurrentSelection(groupIdentifier: String) = {
    val selectedComponents = selectionManager.getSelection.getSelectedComponents
    // And group them
    if (selectedComponents.size() > 0) {
      // TODO (discuss): creating groups of size 1 does not make much sense, does it?
      val groupedComponent = model.group(selectedComponents, groupIdentifier);
      // select the group
      val cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(Collections.singletonList(groupedComponent))
      jGraph.setSelectionCells(cellsForGraphComponents.toArray())
    }
  }


  private def unGroupCurrentSelection() = {
    val selectedComponents = selectionManager.getSelection.getSelectedComponents
    
    // ungroup selected components
    val ungroupedComponents = new ArrayList[GraphComponent]()
    for (graphComponent <- selectedComponents) {
      if (graphComponent.isInstanceOf[CompositeGraphComponent]) {
        ungroupedComponents.addAll(model.ungroup(graphComponent.asInstanceOf[CompositeGraphComponent]))
      }
    }
    // select the components that are ungrouped
    val cellsForGraphComponents = jGraphModelAdapter.getCellsForGraphComponents(ungroupedComponents)
    jGraph.setSelectionCells(cellsForGraphComponents.toArray())
  }

  private def selectAll() {
    selectionTable.selectAll()
  }

  private def selectNone() {
    selectionTable.clearSelection()
  }

  private def makeSelectionVisible() = {
    val componentsToUnhide = selectionManager.getSelection.getSelectedComponents
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
  }

  private def makeSelectionInvisible() = {
    val componentsToHide = selectionManager.getSelection.getSelectedComponents
    model.changeComponentsVisibility(componentsToHide.toList, false)
  }

  private def expandOutgoingDependenciesForSelection() = {
    val componentsToUnhide = ListBuffer[GraphComponent]()
    val selectedComponents = selectionManager.getSelection.getSelectedComponents
    for (graphComponent <- selectedComponents) {
      for (dependency <- graphComponent.getOutgoingDependencies()) {
        componentsToUnhide += dependency.getTargetComponent
      }
    }
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
    selectionManager.updateSelection(null, selectedComponents)
  }

  private def expandIncomingDependenciesForSelection() = {
	val componentsToUnhide = ListBuffer[GraphComponent]()
    val selectedComponents = selectionManager.getSelection.getSelectedComponents
    for (graphComponent <- selectedComponents) {
      for (dependency <- graphComponent.getIncomingDependencies()) {
        componentsToUnhide += dependency.getSourceComponent
      }
    }
    model.changeComponentsVisibility(componentsToUnhide.toList, true)
    selectionManager.updateSelection(null, selectedComponents)
  }

  private def computeDividerLocation() : Int = {
    val rightSize = splitPane.getRightComponent().getPreferredSize().width
    var width = splitPane.getWidth()
    // before the frame becomes visible the initial width returns 0
    if (width == 0) {
      width = FRAME_WIDTH;
    }
    width - rightSize
  }

  def evaluateMenuItemState() {
    menuItemEnableFunctions.foreach { entry =>
      entry._1.setEnabled(entry._2())
    }
  }

  /**
   * Called when another component model is loaded.
   */
  def componentModelChanged() {
    // For some menu items, the enabled/disabled state depends on what functionality the component model suppports, so re-evaluate
    evaluateMenuItemState()
    // If there is a 'legend' window visible, re-create it, as different component models have different legends
    if (visibleLegendWindow != null) {
      visibleLegendWindow.setVisible(false)
      SwingUtilities.invokeLater(new Runnable() {
        def run() { showLegend() }
      })
    }
  }

  def createJMenuItem(title : String, condition : () => Boolean, callback: () => Unit) : JMenuItem = {
    val menuItem = new JMenuItem(title)
    if (condition != null) {
      menuItemEnableFunctions(menuItem) = condition
      menuItem.setEnabled(condition())
    }
    menuItem.addActionListener(createActionListener(callback))
    menuItem
  }

  def createJCheckBoxMenuItem(title : String, condition : () => Boolean, callback: () => Unit) : JMenuItem = {
    val menuItem = new JCheckBoxMenuItem(title, condition())
    menuItem.addActionListener(createActionListener(callback))
    menuItem
  }


  def createJButton(tooltip: String, iconLocation: String, callback: () => Unit): JButton = {
    val button = new JButton()
    if (iconLocation != null) {
	    val buttonIcon = ImageIO.read(this.getClass().getResourceAsStream(iconLocation))
	    button.setIcon(new ImageIcon(buttonIcon))
    } else {
      button.setText(tooltip)
    }
    button.setToolTipText(tooltip)
    button.setMinimumSize(buttonSize)
    button.setMaximumSize(buttonSize)
    button.setPreferredSize(buttonSize)
    button.addActionListener(createActionListener(callback))
    button
  }

  def createActionListener(callback: () => Unit): ActionListener = {
    new ActionListener() {
      def actionPerformed(event: ActionEvent) = {
        callback()
      }
    }
  }

  class PropertiesTableSelectionListener extends SelectionListener {

    def onSelectionChanged(selection: Selection) = {
      val selectedComponents = selectionManager.getSelection.getSelectedComponents
      if (selectedComponents.size > 0) {
        // update properties table selected component
        val selectedComponent = selectedComponents.get(0)
        if (selectedComponent.isInstanceOf[SimpleGraphComponent]) {
          propertiesTableModel.setSelectedComponent(selectedComponent.asInstanceOf[SimpleGraphComponent].getComponent)
        } else {
          // clear selection
          propertiesTableModel.setSelectedComponent(null)
        }
      } else {
        propertiesTableModel.setSelectedComponent(null)
      }
    }
    
  }
  
  abstract class LoadTask() extends SwingWorker[GraphModel, String] with Progress {
    var start : Long = 0
    var end : Long = 0
    var progressCount : Int = 0

    var taskTitle : String = null
    var progressDlg : JDialog = null
    val progressBar = new JProgressBar()
    val progressText = new JLabel()

    {
	  this.taskTitle = "Creating model"
	  progressBar.setIndeterminate(true)
	  setProgressText("Starting...")
    }

    override def setRange(min : Int, max : Int) = {
      SwingUtilities.invokeLater(new Runnable() {
        def run() = {
          progressBar.setIndeterminate(false)
          progressBar.setMinimum(min)
          progressBar.setMaximum(max)
        }
      })
    }

    override def increment() = {
      setCurrentProgress(progressCount + 1)
    }

    override def setCurrentProgress(progress : Int) = {
      progressCount = progress
      publish("" + progress + "/" + progressBar.getMaximum())
    }

    override def setProgressText(message : String) = {
      publish(message)
    }

    override def setIndeterminate(on : Boolean) = {
      progressBar.setIndeterminate(on)
    }

    override def cancelled() : Boolean = {
      isCancelled()
    }

    def getComponentModel() : ComponentModel
    
    def isComponentsDefaultVisible() : Boolean

    override def doInBackground() : GraphModel = {
      start = System.currentTimeMillis()
      val newModel = new GraphModel(getComponentModel(), labelConverter, isComponentsDefaultVisible)
      setProgressText("Preparing model...")
      newModel.prepare()
      setProgressText("Updating graph...")
      newModel
    }

    override def process(messages : java.util.List[String]) = {
      // Only show progress after some time
      if ((System.currentTimeMillis() - start) > 100) {
        if (progressDlg == null) {
          // This might seem strange, as this method is called on the EDT, but when invoke directly, the
          // call will block on the setVisible of the modal dialog and this call will thus not return. In
          // some cases (when the worker finishes very fast), this will lead to a progress dialog that
          // does not close when done.
          // Strange enough, this does not happen always (as you would expect); it seems the EDT is handling
          // long blocking modal dialogs in a 'smarter' way...
          SwingUtilities.invokeLater(new Runnable() {
            def run() = {
              showProgressDialog()
            }
          })
        }
        progressBar.setValue(progressCount)
        // Find latest (non-empty) message
        breakable {
          if (messages.size > 0) {
	          for (i <- messages.size to 1 by -1) {
	            val current = messages.get(i - 1)
	            if (current != null && current.length() > 0) {
	        	  progressText.setText(current);
	              break
	            }
	          }
          }
        } // end breakable
      }
    }

    private def showProgressDialog() = {
      if (!isDone()) {
        progressDlg = new JDialog(GraphFrame.this, taskTitle, true)
        progressDlg.setLocationRelativeTo(GraphFrame.this)

        val panel = new JPanel()
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20))
        val boxLayout = new BoxLayout(panel, BoxLayout.PAGE_AXIS)
        panel.setLayout(boxLayout)

        panel.add(progressBar)
        progressText.setPreferredSize(new Dimension(200, progressText.getPreferredSize().height))
        progressText.setAlignmentX(0.5f)
        panel.add(progressText)

        val cancelButton = new JButton("cancel")
        cancelButton.setAlignmentX(0.5f)
        cancelButton.addActionListener(new ActionListener() {
            def actionPerformed(actionEvent : ActionEvent) {
                cancel(true)
            }
        });
        panel.add(cancelButton)

        progressDlg.add(panel)
        progressDlg.pack()
        progressDlg.setVisible(true)
      }
    }

    override def done() = {
      if (debug) logger.debug("creating model took " + (System.currentTimeMillis() - start) +  " ms")  // TODO: use logger
	  val startUI = System.currentTimeMillis()
	  if (! isCancelled()) {
        try {
          model = get()
          val models = System.currentTimeMillis()
          jGraphModelAdapter = new JGraphModelAdapter(GraphFrame.this, model)
          if (debug) logger.debug("creating JGraphModel took " + (System.currentTimeMillis()-models) + " ms")
          jGraph.setModel(jGraphModelAdapter)
          graphScrollPane.setViewportView(jGraph)
          selectionTableModel.setModel(model)
          updateSelectionTableColumnModel
          selectionTableModel.refresh
          applyForceDirectedLayout()
          componentModelChanged()
        } catch {
          case ex : InterruptedException => {
            // (thrown by SwingWorker.get()) - Impossible: we're done
          }
          case ex: ExecutionException => {
            if (ex.getCause().isInstanceOf[ZipException]) {
              JOptionPane.showMessageDialog(GraphFrame.this, "Invalid jar file.", "Error", JOptionPane.ERROR_MESSAGE)
            } else {
              if (debug) {
                logger.debug("An error occurred during loading: " + ex);
                ex.printStackTrace()
              }
              JOptionPane.showMessageDialog(GraphFrame.this, "A surprising new error has occurred.", "Error", JOptionPane.ERROR_MESSAGE)
            }
          }
        } // end catch
      }

      end = System.currentTimeMillis()
      if (progressDlg != null && end - start < 500) {
        // Avoid flashing, display progress dialog a little longer
        try {
          Thread.sleep(500)
        } catch {
          case ex: InterruptedException => {}
        }
      }

      if (progressDlg != null) {
        progressDlg.setVisible(false)
        progressDlg.dispose()
      }

	  val endUI = System.currentTimeMillis();
	  if (debug) {
	    logger.debug("creating UI took " + (endUI - startUI) + " ms")
	    logger.debug("Loading took " + (end-start) + " ms")
	  }

      val result = model.getComponentModel().getCreationResult()
      if (result != null && result.trim().length() > 0) {
        JOptionPane.showMessageDialog(GraphFrame.this, result, "result", JOptionPane.OK_OPTION)
      }

    } // end of done()
  } // end of LoadTask
  
  /**
   * Cell renderer that displays a tool tip with the actual value.
   */
  class TooltipCellRenderer extends DefaultTableCellRenderer {
     override def getTableCellRendererComponent(
                        table : JTable, value : Object,
                        isSelected : Boolean, hasFocus : Boolean,
                        row : Int, column : Int) : Component = {
       val c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column).asInstanceOf[JLabel]
       val tooltipValue = value.toString()
       c.setToolTipText(tooltipValue)
       c
     }
  }

}
