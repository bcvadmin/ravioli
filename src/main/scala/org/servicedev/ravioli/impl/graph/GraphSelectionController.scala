/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import scala.collection.mutable.ListBuffer
import org.jgraph.JGraph
import org.jgraph.graph.DefaultGraphCell
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import scala.collection.JavaConversions._

/**
 * Controller responsible for handling selection changed events from the jGraph through handleGraphSelectionChanged
 * and for handing onSelectionChanged events from the SelectionManager.
 */
class GraphSelectionController(jGraph: JGraph, selectionManager: SelectionManager) extends SelectionListener {

  /**
   * Handle a graph selection change, i.e. someone selected one or multiple items in the jGraph.
   */
  def handleGraphSelectionChanged() = {
    val selectedComponents = ListBuffer[GraphComponent]()
    for (cell <- jGraph.getSelectionCells()) {
      if (cell.isInstanceOf[DefaultGraphCell]) {
        val graphCell = cell.asInstanceOf[DefaultGraphCell];
        val userObject = graphCell.getUserObject()
        if (userObject.isInstanceOf[GraphComponent]) {
          selectedComponents += userObject.asInstanceOf[GraphComponent];
        }
      }
    }
    selectionManager.updateSelection(GraphSelectionController.this, selectedComponents.toList)
  }

  override def onSelectionChanged(selection: Selection) = {
    val model = jGraph.getModel().asInstanceOf[JGraphModelAdapter]
    val cells = model.getCellsForGraphComponents(selection.getSelectedComponents).toArray()
    jGraph.setSelectionCells(cells)
    if (cells.length > 0) {
      val firstSelectedCell = cells(0)
      jGraph.scrollCellToVisible(firstSelectedCell)
    }
  }

}