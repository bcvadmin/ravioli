/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.impl.graph

import java.awt.Color
import java.awt.Font
import java.awt.geom.Rectangle2D
import java.util.ArrayList
import java.util.Arrays
import java.util.Collection
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import javax.swing.BorderFactory
import org.jgraph.event.GraphModelEvent
import org.jgraph.event.GraphModelListener
import org.jgraph.graph._
import org.jgraph.graph.AttributeMap.SerializableRectangle2D
import org.servicedev.ravioli.impl.graph.model.CompositeGraphComponent
import org.servicedev.ravioli.impl.graph.model.GraphComponent
import org.servicedev.ravioli.impl.graph.model.GraphDependency
import org.servicedev.ravioli.impl.graph.model.GraphModelChangeEvent
import org.servicedev.ravioli.impl.graph.model.GraphModelChangeListener
import scala.Option
import scala.collection.mutable.ListBuffer
import scala.collection.JavaConversions._
import java.util.Hashtable
import javax.swing.JComponent
import javax.swing.JFrame

/**
 * Adapter that wraps a GraphModel into a JGraphModel.
 */
class JGraphModelAdapter(frame: JFrame, graphModel: org.servicedev.ravioli.impl.graph.model.GraphModel) extends DefaultGraphModel with GraphModelChangeListener with GraphModelListener {

  val DEFAULT_NODE_COLOR: Color = Color.decode("#FFA500")
  val GROUP_NODE_COLOR: Color = Color.decode("#D2691E")
  val DEFAULT_EDGE_COLOR: Color = Color.decode("#7AA1E6")
  val MAX_NODE_WIDTH = 400

  private val nodeMap: Map[String, DefaultGraphCell] = new HashMap[String, DefaultGraphCell]
  private val cachedGraphComponents: Map[String, GraphComponent] = new HashMap[String, GraphComponent]

  graphModel.addGraphModelChangeListener(this)
  addGraphModelListener(this)
  updateComponents

  def refreshNodes {
    cellsChanged(nodeMap.values.toArray)
  }

  private def reset {
    remove(getRoots.toArray)
    nodeMap.clear
    cachedGraphComponents.clear
  }

  private def addJGraphNode(component: GraphComponent) {
    if (!nodeMap.containsKey(component.getIdentifier)) {
      val cell: DefaultGraphCell = new DefaultGraphCell(component)
      cell.add(new DefaultPort)
      cell.setUserObject(component)
      val attributeMap = new AttributeMapWrapper()
      attributeMap.put(cell, getNodeAttributes(cell))
      insert(Array[AnyRef](cell), attributeMap.get, null, null, null)
      nodeMap.put(component.getIdentifier, cell)
    }
  }

  private def addJGraphNodes(components: Collection[GraphComponent]) {
    val cells: Set[DefaultGraphCell] = new HashSet[DefaultGraphCell]
    val attributeMap = new AttributeMapWrapper
    for (component <- components) {
      if (!nodeMap.containsKey(component.getIdentifier)) {
        val cell: DefaultGraphCell = new DefaultGraphCell(component)
        cell.add(new DefaultPort)
        cell.setUserObject(component)
        nodeMap.put(component.getIdentifier, cell)
        cells.add(cell)
        attributeMap.put(cell, getNodeAttributes(cell))
      }
    }
    insert(cells.toArray, attributeMap.get(), null, null, null)
  }

  private def addJGraphEdge(dependency: GraphDependency) {
    val set: ConnectionSet = new ConnectionSet
    val theEdge: DefaultEdge = new DefaultEdge(dependency)
    var from: DefaultGraphCell = null
    var to: DefaultGraphCell = null
    val fromComponent: GraphComponent = dependency.getSourceComponent
    val fromVertexId: String = fromComponent.getIdentifier
    val toComponent: GraphComponent = dependency.getTargetComponent
    val toVertexId: String = toComponent.getIdentifier
    if (!nodeMap.containsKey(fromVertexId)) {
      addJGraphNode(fromComponent)
    }
    from = nodeMap.get(fromVertexId)
    if (!nodeMap.containsKey(toVertexId)) {
      addJGraphNode(toComponent)
    }
    to = nodeMap.get(toVertexId)
    set.connect(theEdge, from.getChildAt(0).asInstanceOf[DefaultPort], to.getChildAt(0).asInstanceOf[DefaultPort])
    val edgeAttributeMap = new AttributeMapWrapper
    edgeAttributeMap.put(theEdge, getEdgeAttributes(theEdge))
    insert(Array[AnyRef](theEdge), edgeAttributeMap.get, set, null, null)
  }

  private def addJGraphEdges(dependencies: Collection[GraphDependency]) = {
    val set: ConnectionSet = new ConnectionSet
    val attributes = new Hashtable[Object, AttributeMap]()
    val objects = ListBuffer[AnyRef]()
    for (dependency <- dependencies) {
      val theEdge = new DefaultEdge(dependency)
      var from: DefaultGraphCell = null
      var to: DefaultGraphCell = null
      val fromComponent = dependency.getSourceComponent
      val fromVertexId = fromComponent.getIdentifier
      val toComponent = dependency.getTargetComponent
      val toVertexId = toComponent.getIdentifier
      if (!nodeMap.containsKey(fromVertexId)) {
        addJGraphNode(fromComponent)
      }
      from = nodeMap.get(fromVertexId)
      if (!nodeMap.containsKey(toVertexId)) {
        addJGraphNode(toComponent)
      }
      to = nodeMap.get(toVertexId)
      set.connect(theEdge, from.getChildAt(0).asInstanceOf[DefaultPort], to.getChildAt(0).asInstanceOf[DefaultPort])
      attributes.put(theEdge, getEdgeAttributes(theEdge))
      objects.add(theEdge)
    }
  	insert(objects.toArray[Object], attributes, set, null, null)
  }

  private def getEdgeAttributes(edge: DefaultEdge): AttributeMap = {
    val graphDependency: GraphDependency = edge.getUserObject.asInstanceOf[GraphDependency]
    val eMap: AttributeMap = new AttributeMap
    GraphConstants.setDisconnectable(eMap, false)
    GraphConstants.setLineEnd(eMap, GraphConstants.ARROW_TECHNICAL)
    GraphConstants.setEndFill(eMap, true)
    GraphConstants.setEndSize(eMap, 10)
    GraphConstants.setForeground(eMap, Color.decode("#25507C"))
    GraphConstants.setFont(eMap, GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12))
    GraphConstants.setLineColor(eMap, getEdgeColorForPresentationType(graphDependency.getPresentationType))
    GraphConstants.setEditable(eMap, false)
    return eMap
  }

  private def getNodeAttributes(cell: DefaultGraphCell): AttributeMap = {
    val graphComponent: GraphComponent = cell.getUserObject.asInstanceOf[GraphComponent]
    val presentationType: Option[String] = graphComponent.getPresentationType
    val font = GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12)
    val vMap: AttributeMap = new AttributeMap
    if (graphComponent.getXPosition == 0 && graphComponent.getYPosition == 0) {
      var width = frame.getFontMetrics(font).stringWidth(graphComponent.getLabel) + 20
      if (width > MAX_NODE_WIDTH) {
        width = MAX_NODE_WIDTH
      }
      GraphConstants.setBounds(vMap, new Rectangle2D.Double(50, 50, width, 30))
    }
    else {
      val x: Double = graphComponent.getXPosition
      val y: Double = graphComponent.getYPosition
      GraphConstants.setBounds(vMap, new Rectangle2D.Double(x, y, graphComponent.getWidth, graphComponent.getHeight))
    }
    GraphConstants.setBorder(vMap, BorderFactory.createRaisedBevelBorder)
    if (graphComponent.isInstanceOf[CompositeGraphComponent]) {
      GraphConstants.setBackground(vMap, GROUP_NODE_COLOR)
    } else {
      GraphConstants.setBackground(vMap, getNodeBackgroundColorForPresentationType(presentationType))
    }
    GraphConstants.setForeground(vMap, Color.white)
    GraphConstants.setFont(vMap, font)
    GraphConstants.setOpaque(vMap, true)
    GraphConstants.setEditable(vMap, false)
    GraphConstants.setSizeableAxis(vMap, GraphConstants.X_AXIS)
    
    return vMap
  }

  def getNodeBackgroundColorForPresentationType(presentationType: Option[String]): Color = {
    if (presentationType.isDefined) {
	    if (presentationType.get == "dm-component") {
	      return Color.decode("#A80000")
      } else if (presentationType.get == "osgi-service") {
        return Color.decode("#A8A8A8")
      } else if (presentationType.get == "osgi-service") {
        return DEFAULT_NODE_COLOR
      }
    }
    return DEFAULT_NODE_COLOR
  }

  def getEdgeColorForPresentationType(presentationType: Option[String]): Color = {
    if (presentationType.isDefined) {
      if (presentationType.get == "package") {
        return Color.decode("#99ACFF")
      }
      else if (presentationType.get == "bundle") {
        return Color.decode("#626FA1")
      }
      else if (presentationType.get == "fragment-host") {
        return Color.decode("#62A164")
      }
      else if (presentationType.get == "implements") {
        return Color.decode("#C0C0C0")
      } 
      else if (presentationType.get == "requires") {
        return Color.decode("#0000FF")
      } 
      else if (presentationType.get == "required-dependency-unavailable") {
        return Color.RED
      }
      else if (presentationType.get == "optional-dependency-unavailable") {
        return Color.ORANGE
      }
    }
    return DEFAULT_EDGE_COLOR
  }

  def onGraphModelChange(event: GraphModelChangeEvent) {
    if (event.getType eq GraphModelChangeEvent.ChangeTypeEnum.GRAPH_RESET) {
      refresh
    }
    else {
      updateComponents
    }
  }

  private def updateComponents {
    val componentsToRemove: Set[String] = new HashSet[String]
    val componentsToAdd: Set[GraphComponent] = new HashSet[GraphComponent]
    for (graphComponentIdentifier <- cachedGraphComponents.keySet) {
      if (graphModel.getGraphComponent(graphComponentIdentifier) == null) {
        componentsToRemove.add(graphComponentIdentifier)
      }
      else {
        val cachedGraphComponent: GraphComponent = cachedGraphComponents.get(graphComponentIdentifier)
        val graphComponent: GraphComponent = graphModel.getGraphComponent(graphComponentIdentifier)
        if (!graphComponentsAreEqual(graphComponent, cachedGraphComponent)) {
          componentsToRemove.add(graphComponentIdentifier)
          if (graphComponent.isVisible) {
            componentsToAdd.add(graphComponent)
          }
        }
      }
    }
    if (componentsToRemove.size() == nodeMap.size()) {
      // just remove all
      reset
    } else {
    	val nodesToRemove = ListBuffer[String]()
	    for (graphComponentIdentifier <- componentsToRemove) {
	      if (nodeMap.containsKey(graphComponentIdentifier)) {
	        nodesToRemove += graphComponentIdentifier
	      }
	    }
    	removeGraphNodes(nodesToRemove)
    }
    for (graphComponent <- graphModel.getGraphComponents) {
      if (!cachedGraphComponents.containsKey(graphComponent.getIdentifier)) {
        if (graphComponent.isVisible) {
          componentsToAdd.add(graphComponent)
        }
      }
    }
    cachedGraphComponents.clear
    for (graphComponent <- graphModel.getGraphComponents) {
      cachedGraphComponents.put(graphComponent.getIdentifier, graphComponent.createCopy)
    }
    addGraphNodes(componentsToAdd)
  }

  private def graphComponentsAreEqual(graphComponent: GraphComponent, cachedGraphComponent: GraphComponent): Boolean = {
    return graphComponent.isVisible == cachedGraphComponent.isVisible
  }

  private def addGraphNode(graphComponent: GraphComponent) {
    addJGraphNode(graphComponent)
    for (dependency <- graphComponent.getOutgoingDependencies) {
      val targetIdentifier: String = dependency.getTargetComponent.getIdentifier
      if (nodeMap.containsKey(targetIdentifier)) {
        addJGraphEdge(dependency)
      }
    }
    for (dependency <- graphComponent.getIncomingDependencies) {
      val sourceIdentifier: String = dependency.getSourceComponent.getIdentifier
      if (nodeMap.containsKey(sourceIdentifier)) {
        addJGraphEdge(dependency)
      }
    }
  }

  private def addGraphNodes(components: Collection[GraphComponent]) {
    addJGraphNodes(components)
    val edgesToAdd: Set[GraphDependency] = new HashSet[GraphDependency]
    for (graphComponent <- components) {
      for (dependency <- graphComponent.getOutgoingDependencies) {
        val targetIdentifier: String = dependency.getTargetComponent.getIdentifier
        if (nodeMap.containsKey(targetIdentifier)) {
          edgesToAdd.add(dependency)
        }
      }
      for (dependency <- graphComponent.getIncomingDependencies) {
        val sourceIdentifier: String = dependency.getSourceComponent.getIdentifier
        if (nodeMap.containsKey(sourceIdentifier)) {
          edgesToAdd.add(dependency)
        }
      }
    }
    addJGraphEdges(edgesToAdd)
  }

  private def removeGraphNode(graphComponentIdentifier: String) {
    val cell: DefaultGraphCell = nodeMap.get(graphComponentIdentifier)
    val edges: Set[_] = DefaultGraphModel.getEdges(this, Array[AnyRef](cell))
    remove(edges.toArray)
    remove(Array[AnyRef](cell))
    nodeMap.remove(graphComponentIdentifier)
  }
  
  private def removeGraphNodes(graphComponentIdentifiers : ListBuffer[String]) = {
    val objects = ListBuffer[Object]()
    for (graphComponentIdentifier <- graphComponentIdentifiers) {
    	val cell: DefaultGraphCell = nodeMap.get(graphComponentIdentifier)
    	val edges: Set[_] = DefaultGraphModel.getEdges(this, Array[AnyRef](cell))
    	objects += cell
    	objects ++= edges.toArray()
    	nodeMap.remove(graphComponentIdentifier)
    }
    remove(objects.toArray[Object])
  }

  def graphChanged(e: GraphModelEvent) {
    val source: AnyRef = e.getSource
    var changedObjects = e.getChange.getChanged.toList
    if (e.getChange.getRemoved != null) {
      changedObjects = changedObjects diff e.getChange.getRemoved()
    }
    if (e.getChange.getInserted != null) {
      changedObjects = changedObjects diff e.getChange.getInserted
    }
    for (changedObject <- changedObjects if changedObject.isInstanceOf[DefaultGraphCell] && changedObject.asInstanceOf[DefaultGraphCell].getUserObject.isInstanceOf[GraphComponent]) {
      val cell = changedObject.asInstanceOf[DefaultGraphCell]
      val bounds: AttributeMap.SerializableRectangle2D = cell.getAttributes.get("bounds").asInstanceOf[AttributeMap.SerializableRectangle2D]
      (cell.getUserObject.asInstanceOf[GraphComponent]).setBounds(bounds.x, bounds.y, bounds.width, bounds.height)
    }
  }

  def getCellsForGraphComponents(graphComponents: Collection[GraphComponent]): List[GraphCell] = {
    val cells: List[GraphCell] = new ArrayList[GraphCell]
    for (component <- graphComponents) {
      if (nodeMap.containsKey(component.getIdentifier)) {
        cells.add(nodeMap.get(component.getIdentifier))
      }
    }
    return cells
  }

  def refresh {
    reset
    updateComponents
  }
  
}