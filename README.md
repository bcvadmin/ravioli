# Ravioli

## Overview

**Ravioli** - _no spaghetti_ -

Ravioli is a graphical visualisation tool for OSGi bundle and service dependencies.

![Ravioli](https://bitbucket.org/uiterlix/ravioli/wiki/ravioli.png)

## Status

As of the 0.3 version, Ravioli can visualize (static) bundle dependencies as well as (dynamic) Dependency Manager Service dependencies. The static view wil be pretty easy to understand: bundles depend on others because they use (import) packages exported by others, or because a bundle as an explicit (fragment-host or require-bundle) dependency on another. The dynamic view might be a bit hard to understand, unless you're an experienced DM user; which will usually be the case if your application uses the Dependency Manager in the first place (if not, this view will be of little or no use, as it will only display services). Make sure you use the legend (Help -> Legend) to find out what the different colors mean. Currently, the only available UI is a Java Swing interface, but we're working hard to have a web-based UI too.

To run the tool, you can either deploy it (the `ravioli-0.x.jar`) as a bundle in an OSGi container (see details below) or run it as a stand-alone application (use `ravioli-standalone-0.x.jar`). When run in an OSGi container, the tool will show the dependencies of all bundles deployed in the OSGi framework. When run stand-alone, you can use the "Scan bundle manifests" menu option in the File menu to scan bundle manifest files and show their dependencies. Of course, dynamic (service) dependencies can only be visualized when running in an OSGi container. Even though the project started not long ago, the tool is fully functional and stable (the released versions, that is ;-)). However, if you encounter any problem, find a bug or have suggestions for improvements, do not hesitate to contact us at <ravioli-no-spaghetti@gmail.com>, we're eager to hear some feedback!

## Requirements

The following list of requirements gives an idea of what we intent to do with the tool in the long run. To see which features are implemented already, see the  [Release notes](https://bitbucket.org/uiterlix/ravioli/src/master/Releases.md).

### Functional requirements
- Graphical visualisation of (static) bundle and (dynamic) service dependencies. Bundle dependencies are calculated from both Require-Bundle and Import-Package manifest headers. Service dependencies are obtained from the Components registered with the Apache Felix DependencyManager.
- Allow the users to re-arrange the nodes in the dependency graph to their liking.
- Allow removal of nodes from the graph.
- Save/Open of dependency graphs.
- Export dependency graph to printable format (PDF/PNG).
- Choice of graph depth when inspecting a node.
- Add incoming/outgoing depedencies to graph function for a node.
- Option to inspect a bundle's manifest headers.
- Create projections by grouping nodes together into a (named) logical node for which the dependencies are determined by the participants of the group.

### Non functionals
- Fit for enterprise size applications. Applications with thousands of runtime components should be no problem; neither for determining the dependencies nor for visualizing them.

## Building
Gradle is used as build tool. Execute the following command to build and test:

    ./gradlew assemble check

## Deploy
For deploying the bundle in an OSGi container, you'll need a scala library bundle too. Download scala from <http://www.scala-lang.org/download/2.10.3.html> and deploy the `scala-library` as bundle in your OSGi framework. Starting from Ravioli version 0.3, you'll need to install the Felix Dependency Manager too (simply download the [jar](http://ftp.nluug.nl/internet/apache//felix/org.apache.felix.dependencymanager-3.1.0.jar) and install it). Build or download the `ravioli-0.2.jar` bundle and deploy it; as soon as the bundle starts, a window will appear that displays the (static) bundle dependencies between all bundles that are present in the OSGi container.

The stand-alone version does not have any external dependencies and can be run with `java -jar ravioli-standalone-0.2.jar` or by double clicking the jar file (depends on OS).

## Development
Please refer to the [Development notes](https://bitbucket.org/uiterlix/ravioli/src/master/Development.md) if you want to know more about the development environment.


## Feedback
If you run into a bug, have problem using Ravioli or have any other suggestion for improvement, feel free to add an issue at <https://bitbucket.org/uiterlix/ravioli/issues> or post on our [Google group](https://groups.google.com/forum/#!forum/ravioli-forum). Thanks for your cooperation.

## License
This Project is Licensed under LGPL.

## Acknowledgements
YourKit is kindly supporting this open source project with its full-featured Java Profiler.
YourKit, LLC is the creator of innovative and intelligent tools for profiling
Java and .NET applications. Take a look at YourKit's leading software products:
[YourKit Java Profiler](http://www.yourkit.com/java/profiler/index.jsp) and
[YourKit .NET Profiler](http://www.yourkit.com/.net/profiler/index.jsp).
